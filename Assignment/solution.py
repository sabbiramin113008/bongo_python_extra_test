# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 24 Oct 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""

'''
Problem: Suppose we are implementing a calendar system, which will take following inputs

Person_1_day, person_2_day, day_start_time, day_end_time, duration.

Your task is to find our free time slots which are available
 to conduct the meeting based on duration.

Sample Input:
person_1_day = [“12:00-12:30”, “2:00-3:00“]
person_2_day = [“9:00-12:00”, “2:00-3:30“]
day_start = “8:00”
day_end = “5:00”
duration = 60  // in minutes

Output of this would be:
 [“8:00-9:00”, “12:30-2:00”, “3:30-5:00”]


Please implement a solution, upload to any code repository (ex: github) and submit url to mirza.asif@bongobd.com.

Note:
Code quality should be production ready.
There should be proper unit tests, with different edge cases.


'''


class NoEndTimeSpecifiedError(IndexError):
    pass


class NoStartTimeSpecifiedError(IndexError):
    pass


class TimeUtil:
    def __init__(self):
        self.time_mapper = {

        }
        for i in range(1, 25):
            m = 60 * i
            for j in [-3, -2, -1, 1, 2, 3]:
                k = m + j
                self.time_mapper[str(k)] = m

    def to_minutes(self, str_time):
        try:
            h = str(str_time).split(':')[0]
        except IndexError as e:
            raise NoStartTimeSpecifiedError('No Starttime Specified')
        try:
            m = str(str_time).split(':')[1]
        except IndexError:
            raise NoEndTimeSpecifiedError('No Endtime Specified')
        return int(h) * 60 + int(m)

    def calculate(self, s_time, e_time):
        return [i for i in range(s_time, e_time + 1)]

    def pocket(self, str_time):
        try:
            s_time = self.to_minutes(str_time.split('-')[0])
        except NoStartTimeSpecifiedError as e:
            raise NoStartTimeSpecifiedError('No Starttime Specified')

        try:
            e_time = self.to_minutes(str_time.split('-')[1])
        except NoEndTimeSpecifiedError as e:
            raise NoEndTimeSpecifiedError('No Endtime Specified')

        if s_time > e_time:
            raise StartingTimeBiggerThanEndingtimeError('Starting Time is Bigger')

        return self.calculate(s_time, e_time)

    def to_time(self, item):
        s_time = self.time_mapper.get(str(item[0]), item[0])
        e_time = self.time_mapper.get(str(item[-1]), item[-1])

        ss_time = "%d:%02d" % (divmod(s_time, 60))
        ee_time = "%d:%02d" % (divmod(e_time, 60))
        return ss_time, ee_time


class StartingTimeBiggerThanEndingtimeError(BaseException):
    pass


class NoOrEmptyScheduleCardFoundError(BaseException):
    pass


class Schedule:
    def __init__(self, person_one, person_two, day_start_time, day_end_time, meeting_threshold):
        self.occupied = list()
        self.person_one = person_one
        if  not self.person_one:
            raise NoOrEmptyScheduleCardFoundError('No or Empty Schedule Card')
        self.person_two = person_two
        if not self.person_two:
            raise NoOrEmptyScheduleCardFoundError('No or Empty Schedule Card')
        self.time_util = TimeUtil()
        self.start = self.time_util.to_minutes(day_start_time)
        self.end = self.time_util.to_minutes(day_end_time) + 1

        if self.end < self.start:
            raise StartingTimeBiggerThanEndingtimeError('Starting Time is Bigger')

        self.meeting_threshold = meeting_threshold

        for item in self.person_one:
            self.occupied.extend(self.time_util.pocket(item))
        for item in self.person_two:
            self.occupied.extend(self.time_util.pocket(item))
        sorted(self.occupied)
        self.approximated = list()
        for i in self.occupied:
            if self.time_util.time_mapper.get(str(i), None):
                self.approximated.append(self.time_util.time_mapper.get(str(i)))
            else:
                self.approximated.append(i)
        list(set(self.approximated))

    def calculate(self):
        result = list()
        bag = []
        trunk = list()
        # for i in range(8 * 60, 17 * 60 + 1):
        for i in range(self.start, self.end):
            if i not in self.approximated:
                bag.append(i)
            else:
                bag_copy = bag.copy()
                trunk.append(bag_copy)
                bag.clear()
        trunk.append(bag)
        for i in trunk:
            if len(i) >= self.meeting_threshold:
                # print(i[0], i[-1])
                response = self.time_util.to_time(i)
                print(response)
                result.append(response)
        return result


if __name__ == '__main__':
    time_format = '%H:%M'
    str_start_time = '8:00'
    str_end_time = '17:00'
    meeting_threshold = 30

    person_one = ['12:00-12:30', '14:00-15:00']
    person_two = ['9:00-12:00', '14:00-15:30']

    schedule = Schedule(person_one, person_two, str_start_time, str_end_time, meeting_threshold)
    schedule.calculate()
