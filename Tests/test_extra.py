# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 24 Oct 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""

import unittest

from Assignment.solution import *


class TestMe(unittest.TestCase):
    equal_output = [
        ('8:00', '9:00'),
        ('12:31', '14:00'),
        ('15:31', '17:00'),
    ]
    time_format = '%H:%M'
    str_start_time = '8:00'
    str_end_time = '17:00'
    meeting_threshold = 30

    person_one = ['12:00-12:30', '14:00-15:00']
    person_two = ['9:00-12:00', '14:00-15:30']

    empty_card = NoOrEmptyScheduleCardFoundError

    def test_equal(self):
        str_start_time = '8:00'
        str_end_time = '17:00'
        meeting_threshold = 30

        person_one = ['12:00-12:30', '14:00-15:00']
        person_two = ['9:00-12:00', '14:00-15:30']

        schedule = Schedule(person_one, person_two, str_start_time, str_end_time, meeting_threshold)
        response = schedule.calculate()
        print(type(response))
        self.assertListEqual(response, self.equal_output, 'How Do I Fail')

    def test_NoOrEmptyScheduleCardFoundError(self):
        str_start_time = '8:00'
        str_end_time = '17:00'
        meeting_threshold = 30

        person_one = []
        person_two = ['9:00-12:00', '14:00-15:30']
        with self.assertRaises(NoOrEmptyScheduleCardFoundError):
            schedule = Schedule(person_one, person_two, str_start_time, str_end_time, meeting_threshold)

    def test_StartingTimeBiggerThanEndingtimeError(self):
        str_start_time = '8:00'
        str_end_time = '17:00'
        meeting_threshold = 30

        person_one = ['13:50-12:30', '14:00-15:00']
        person_two = ['9:00-12:00', '14:00-15:30']
        with self.assertRaises(AttributeError):
            schedule = Schedule(person_one, person_two, str_start_time, str_end_time, meeting_threshold)


if __name__ == '__main__':
    unittest.main()
