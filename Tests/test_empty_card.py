# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 24 Oct 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""
import unittest
from Assignment.solution import *

class EmptyCardTest(unittest.TestCase):
    def test_NoOrEmptyScheduleCardFoundError(self):

        str_start_time = '8:00'
        str_end_time = '17:00'
        meeting_threshold = 30

        person_one = []
        person_two = ['9:00-12:00', '14:00-15:30']
        with self.assertRaises(NoOrEmptyScheduleCardFoundError):
            schedule = Schedule(person_one, person_two, str_start_time, str_end_time, meeting_threshold)


if __name__=='__main__':
    unittest.main()